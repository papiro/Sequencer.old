module.exports = function (grunt)
{
	// Project configuration
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
			},
			build: {
				src: 'Project_Files/<%= pkg.name %>.js',
				dest: 'Project_Files/js/min.js/<%= pkg.name %>.min.js'
			}
		},
		jshint: {
			build: ['Gruntfile.js', 'Project_Files/js/<%= pkg.name %>.js']
		},
		less: {
			build: {
				src: 'Project_Files/<%= pkg.name %>.less',
				dest: 'Project_Files/css/<%= pkg.name %>.css'
			}
		},
		watch: {
			options: {
					livereload:9090
				},
			html: {
				files: 'Project_Files/index.html'				
			},
			scripts: {
				files: ['Gruntfile.js', 'Project_Files/<%= pkg.name %>.js'],
				tasks: ['uglify', 'jshint']				
			},
			css: {
				files: 'Project_Files/<%= pkg.name %>.less',
				tasks: 'less'				
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-less');


	grunt.registerTask('default', ['watch', 'less', 'uglify', 'jshint']);
};