function buildCoreAPI() {

	var core = {};

	core.powerOn = function( options ) {

		this.head( options );

		if( options.print ) SEQUENCER.print();

		this.powerOff();
	};

	core.powerOff = function() {
		//core = {};
	};

	core.memory = {};

	core.head = function ( options ) {

/***
**	 The core algorithm consists of three components:
**	 1. The index.
**	 2. The minor.
** 	 3. The major.
***/
		var meta = this.memory.meta = {};

		this.memory.goal = Number( options.index );

		meta.index = 0;

// The first two numbers of the Fibonacci Sequence.
		meta.minor = "0";
		meta.major = "1";

		do{
			this.arm( meta );

			this.heart( meta );

			this.memory.goal--;

		} while ( this.memory.goal );
	};

// Normalize the strings.
	core.arm = function( meta ) {
		while( meta.minor.length !== meta.major.length ) {
			meta.minor = "0" + meta.minor;

			if(Math.abs(meta.minor.length-meta.major.length)>2) {
				console.error("ERROR: Minor greater than major!?");
				break;
			}
		}
	};

	core.heart = function( meta ) {	

		this.memory.carryTheOne = false;

// Start from the end and work towards the beginning.
		var end = meta.major.length,
			temp = "";

		for( --end; end >= 0; end-- ){     
			temp = synthesize( meta, temp );
		}

		meta.minor = meta.major;
		meta.major = ( this.memory.carryTheOne? "1" : "" ) + temp;
		meta.index++;

		function synthesize( meta, temp ) {

			var number = Number( meta.minor[end] ) + Number( meta.major[end] ) + ( core.memory.carryTheOne? 1 : 0 );

			if( number >= 10 ) core.memory.carryTheOne = true;
			else			   core.memory.carryTheOne = false;

			if(core.memory.goal===1) debugger;
			return ( String( number % 10 ) + temp );
		}

	};

	return core;
}