
SEQUENCER = ( function() {

// Build the internal API	
	var core = buildCoreAPI();

// The external API
	return {
		generate : generate,
		print : print
	}

// Generate the Fibonacci Sequence.
// Do all necessary type and input checking here so we don't
// tie up the rest of the body with checkpoints.
	function generate( options ) {

		options.command === "on"? core.powerOn( options ) : 
		options.command === "off"? core.powerOff() : 
		(function( command ) {
			console.error( options.command + " is not a valid parameter.  Please enter \"on\" or \"off\"." );
		} )( options );

	}	

	function print() {
		console.log("Index: " + core.memory.meta.index + "\n" +
					"Major: " + core.memory.meta.major + "\n" +
					"Minor: " + core.memory.meta.minor + "\n");
	}	

} )();


// Bind the UI events...
document.addEventListener("DOMContentLoaded", function() {

		window.DEBUG = "";

	var beginCalculation = document.getElementById("submitIndex"),
		optionInputs = document.getElementsByClassName("option");

	var options = {};

	beginCalculation.addEventListener( "click", function() {

		for( var i=0; i<optionInputs.length; i++ ) {
			if( optionInputs[i].type === "checkbox" ) options[optionInputs[i].name] = optionInputs[i].checked;
			else options[optionInputs[i].name] = optionInputs[i].value;
		}

		SEQUENCER.generate( options );

	}, false );

} );